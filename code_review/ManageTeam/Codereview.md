### Code Review comments

I would add some state management tool like Redux or other flux flavor. This will help with keep
the way the data is retrieved and sent to the server centralized and consistent, then, the
components will just need to trigger the corresponding action and be connected to the store to
be able to read the data.

Having this will also help with the way the components are receiving the data, IMHO, the
components are defining a very large amount of props, and I think that can lead to some
maintainability problems in the near future.


About the naming convention I don't have too much to say more than I agree with the current one
since was easy to get what each file is meant to do.

I also adding a new file to the project, `BridgeForm` that is just a proposal (snippet of code) to centralize some of the
repeated behaviors that can be found in the components.
This Can be added if the redux (or other tool) is not used.
This Form is a render prop component that handle the post and get methods used in all the files
under the ManageTeam folder.
I think that would be a good idea to identify the repeated patterns and extract them in another
components that can hold the logic avoiding the repeated code.

If the use of redux or even the render prop is not possible then we can consider the use of
React.Context to create a "state store" that can be shared with the ManageTeam components,
this store can hold the data related with the users and enable the access to that data directly in
the component without the need to pass it down (prop drilling), clearing the component API leaving
only the action callers.


I would also advice to try to split the components to get more maintainability and
readability since the current ones are fairly long.

As example of splitting the component and repeated patterns we can take a look at ReassignPanel.

Here the get and post calls are used multiple times, this can be defined outside of the component
as utility functions.

Also the component can be split by thinking in Smart and Dumb components. Here the smart component
will define all the logic and the bumbd component will be only the presentation layer
(`getForm and render` functions)


Questions:

Every single request need to have a cancellation function? Maybe debounce the calls can work
too ?

Validation should be done on all the forms?.
