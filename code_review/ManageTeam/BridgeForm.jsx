import React from 'react';
import PropTypes from 'prop-types';
import Axios from 'axios';

let PostCancelToken = Axios.CancelToken;
let postCancel;
let GetCancelToken = Axios.CancelToken;
let getCancel;


/*
 * This Render Prop component wrapp the common behaviors that can be found
 * in the ManageTeam files that access data or post data to the server
 * The idea is get a shared logic between those components
 * to allow better maintainability and readability
 *
 * Here children must be a function that receive one argument as an object
 * with two attributes, get and post
 * Can be used like
 * <BridgeForm>
 * { ({ get, post }) => (
 *  <Form onSubmit={(data) => post(url, data)} >
 *  ...
 *  ...
 *  ...
 *  </Form>
 * ) }
 * </BridgeForm>
 */

const BridgeForm = ({ children }) => {
    const getCSRF = () => {
      
      let token = "null";
      if (document != undefined && document.querySelector('meta[name=csrf-token]') != null) {
        token = document.querySelector('meta[name=csrf-token]').getAttribute('content');
      return token;
    }

    const post = async (url, data) => {
        token = getCSRF();
        const response = await Axios({
          method: 'post',
          url,
          data,
          headers: {
            'X-CSRF-Token': token,
            'X-Requested-With': 'XMLHttpRequest',
          },
          cancelToken: new PostCancelToken(function executor(c){
            postCancel = c;
          })
        })
        return { response, cancel: postCancel }
        
      }

      const get = async (url) => {
        const response = await Axios({
          method: 'get',
          url,
          headers: {
            Accept: 'application/json',
            'X-Requested-With': 'XMLHttpRequest',
          },
          cancelToken: new GetCancelToken(function executor(c) {
            getCancel = c;
          })
        })
        return { response, cancel: GetCancel }
      }
    return this.props.children({ get, post })
}

BridgeForm.propTypes = {
  children: PropTypes.func.isRequired
}

export default BridgeForm;
