import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Avatar from './Avatar.jsx';

const AvatarGroup = (props) => (
	<div className="circle-group">
		{props.users.map((user, index) => (
			<Avatar
				key={`group-avatar-${index}`}
				user={user}
				thumbnail={user.thumbnail}
				size="sm"
			/>
		))}
	</div>
);

AvatarGroup.propType = {
	users: PropTypes.array,
}

AvatarGroup.defaultProps = {
	users: [],
}

export default AvatarGroup;
